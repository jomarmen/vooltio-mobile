# Aplicación movil para [http://www.vooltio.es/](http://www.vooltio.es/)

Comienza a ahorrar en la factura de la luz conociendo con antelación cuanto está costando la luz ahora mismo y cuánto costará al día siguiente.

Vooltio es una aplicación para conocer cuánto vale la luz, aprovechando que Red Eléctrica de España ha empezado a publicar los nuevos precios de la electricidad en cada una de las horas del día actual y del día siguiente. Con Vooltio tienes acceso a toda esta información de una manera muy fácil y disponible en cualquier momento desde tu dispositivo Android. 

Con Voltio podrás conocer el precio que se está pagando actualmente la luz, conocer las horas en las que la luz es más cara y más barata. Tanto del día actual como del día siguiente. Tendrás todo la serie de precios en una gráfica muy sencilla de entender.

Siguenos en:

- Web - [http://www.vooltio.es/](http://www.vooltio.es/)
- Twitter - [http://www.twitter.com/voltioapp/](http://www.twitter.com/voltioapp/)

Disclaimer

Los datos que se muestran en la aplicación son meramente informativos en ningún caso pueden sustituir al precio que la compañía eléctrica aplique a cada cliente.

# Components

- LungoJS [http://lungo.tapquo.com/](http://lungo.tapquo.com/)
- QuoJS [http://quojs.tapquo.com/](http://quojs.tapquo.com/)
- Monocle [http://monocle.tapquo.com/](http://monocle.tapquo.com/)
- dateJS [http://www.datejs.com/](http://www.datejs.com/)
- Chart.js [http://www.chartjs.org/](http://www.chartjs.org/)

# Credits

  **Creador: Jose Vicente Martinez Mendoza**

  Twitter: [@jomarmen](http://www.twitter.com/jomarmen/)
