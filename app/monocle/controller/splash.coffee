class SplashCtrl extends Monocle.Controller

    events:
        "load section#splashIni"    : "onLoadsplash"

    onLoadsplash: (event) ->
        console.log "onLoadsplash"

        Lungo.Data.Storage.persistent("dateSelected", "today")
        Lungo.Data.Storage.persistent("order_by", "hora")
        Lungo.Data.Storage.persistent("sort", "ASC")

        todayCompressed = Date.parse('today').compressedDate()
        
        Lungo.Service.Settings.error = errorResponse        

        # DATOS PARA HOY

        url = "http://www.vooltio.es/api/v1/serie/precios.json"
        post_data =
            fecha: todayCompressed,
            tarifa: 0

        result = Lungo.Service.get(url, post_data, preciosResponse)

    preciosResponse= (response) ->
        console.log "preciosResponse"

        for precio in response
            hora = precio.tramo.split "-"
            precio = new __Model.Precio tramo: precio.tramo, valor:precio.valor, fecha: precio.fecha, hora: hora[0]
            precio.save()

        url = "http://www.vooltio.es/api/v1/serie/precios.json"
        post_data =
            fecha: Date.parse('tomorrow').compressedDate(),
            tarifa: 0

        result = Lungo.Service.get(url, post_data, preciosManyanaResponse)

    preciosManyanaResponse= (response) ->
        console.log "preciosManyanaResponse"

        console.log response
        for precio in response
            precio = new __Model.Precio tramo: precio.tramo, valor:precio.valor, fecha: precio.fecha
            precio.save()

        Lungo.Router.section("main");

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr.response)

controller_splash = new SplashCtrl "section#splashIni"