class CalculatorFinishCtrl extends Monocle.Controller

    events:
        "load article#calculator-energy-4"  : "onLoad"


    elements:
        "h2#potencia_optima"     :   "potencia_optima"

    onLoad: (event) ->
        console.log "onLoad article#calculator-energy-4"

        potenciaTotal = Lungo.Cache.get("potenciaTotal")
        factor = Lungo.Cache.get("factorSimultaneidad")

        floatFactor = parseFloat(factor.valor)
        floatPotencia = parseFloat(potenciaTotal)

        potencia = ((floatFactor*floatPotencia)/100).toFixed(1)

        tramos = [1.15,2.3,3.45,4.6,5.75,6.9,8.05,9.2,10.35,11.5,14.49]

        tramos_filtrados = tramos.filter (x) -> x > potencia

        potencia =  Math.min.apply @, tramos_filtrados 
        @potencia_optima[0].innerHTML = potencia.toString() + "kW"


controller_calculator = new CalculatorFinishCtrl "article#calculator-energy-4"