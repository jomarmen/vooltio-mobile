class CalculatorResultCtrl extends Monocle.Controller

    events:
        "load article#calculator-energy-3"  : "onLoad"
        "change input#factor"               : "onChangeFactor"
        "mousemove input#factor"            : "onMouseMoveFactor"

    elements:
        "h2#potencia_total"     :   "potencialTotal"        
        "input#factor"          :   "factorRange"
        "h3#factor"             :   "factor"


    onChangeFactor: (event) ->
        console.log "onChangeFactor"

        @factor[0].innerHTML = @factorRange.val() + "%"

        rango = { valor: @factorRange.val().toString()}
        Lungo.Cache.set("factorSimultaneidad", rango)

    onMouseMoveFactor: (event) ->
        console.log "onMouseMoveFactor"

        @factor[0].innerHTML = @factorRange.val() + "%"




    onLoad: (event) ->
        console.log "onLoad article#calculator-energy-3"
        rango = { valor: "30"}
        Lungo.Cache.set("factorSimultaneidad", rango);

        totalPotencia = 0
        for electrodomestico in __Model.Electrodomestico.all()

            if electrodomestico.cuenta > 0
                totalPotencia = totalPotencia + electrodomestico.potencia*electrodomestico.cuenta
                
        potenciaKilowatio = Helpers.toKilowatio(totalPotencia, 3)        
        Lungo.Cache.set("potenciaTotal", potenciaKilowatio)
        @potencialTotal[0].innerHTML = potenciaKilowatio + "kW"



controller_calculator = new CalculatorResultCtrl "article#calculator-energy-3"