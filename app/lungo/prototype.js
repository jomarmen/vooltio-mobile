// For todays date;
Date.prototype.today = function () { 
    return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear();
}

Date.prototype.smartDate = function(){
	day = this.getDay();
    number_day = this.getDate();
    month = this.getMonth();

	var dayNames =  ["dom", "lun", "mar", "mie", "jue", "vie", "sab"];
	var monthNames = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "Ago", "sep", "oct", "nov", "dec"];


    return dayNames[day] + ", " + number_day + " de " + monthNames[month];
}

Date.prototype.compressedDate = function () { 
	return this.getFullYear( )+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) + ((this.getDate() < 10)?"0":"") + this.getDate();    
}



// For the time now
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}

Date.prototype.shortTimeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes();
}